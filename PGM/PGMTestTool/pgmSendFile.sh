#!/bin/sh

chmod a+x pgmFileSend

n=1

while [ -d "$1/Messung$n" ]
do
    n=$((n+1))
done

mkdir "$1/Messung$n"

sleep 10

#IP="$( ip -4 route get 8.8.8.8 | awk {'print $7'} | tr -d '\n' )"

echo $(($(date +%s%N)/1000000)) > "starttime-"$(hostname)"-pgm.txt"
mv "starttime-"* "$1/Messung$n/"

./pgmFileSend -file file.txt -intf "eth0" -addr "239.0.0.1" -udp -udp-port 7500 > log.txt

echo $(($(date +%s%N)/1000000)) > "endtime-"$(hostname)"-pgm.txt"
mv "endtime-"* "$1/Messung$n/"