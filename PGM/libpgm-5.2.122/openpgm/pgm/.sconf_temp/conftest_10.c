

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char eventfd();

int main() {
#if defined (__stub_eventfd) || defined (__stub___eventfd)
  fail fail fail
#else
  eventfd();
#endif

  return 0;
}
