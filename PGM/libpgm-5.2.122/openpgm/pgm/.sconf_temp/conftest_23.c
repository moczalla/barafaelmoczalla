

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char vasprintf();

int main() {
#if defined (__stub_vasprintf) || defined (__stub___vasprintf)
  fail fail fail
#else
  vasprintf();
#endif

  return 0;
}
