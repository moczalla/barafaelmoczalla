#!/bin/sh

# Messen "Anzahl der Messungen" "Anzahl der Teilnehmer" "Protokollkennung"
Messen() {

max=$1
n=$2
proto=$3

for i in `seq 1 $max`
do
    str="$(core-gui -b /home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/core/"$proto"Labor$((n))Recv.imn | grep -Eo '[0-9]{5,5}[.]' | grep -Eo '[0-9]{5,5}')"
    
    timecounter=0
    a=0
    
    while [ $a -lt 1 ]
    do
        t=1
        temp=`expr $n + 1`
        for p in `seq 2 $temp`
        do
            if [ ! -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/endtime-n1((p))-$4.txt" ]
            then
                if [ ! -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/endtime-n$((p))-$4.txt" ]
                then
                    if [ ! -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/n$((p))fileok.txt" ]
                    then
                        t=0
                    else
                        if [ ! -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/n$((p))filenotok.txt" ]
                        then
                            t=0
                        fi
                    fi
                    t=0
                fi
            fi
        done
        
        if [ $t -eq 1 ]
        then
            a=`expr $a + 1`
            sleep 30
        else
            if [ $timecounter -gt 900 ]
            then
                a=`expr $a + 1`
                sleep 30
            else
                timecounter=`expr $timecounter + 30`
                sleep 30
            fi
        fi
    done
    
    core-gui -c $str
    sleep 20
done
}

recvnums="1 5 10 15 20 25 30 40 50 60 70 80 90 100 200 1000"

for inc in $recvnums
do
    # nc Messungen
    Messen 10 $inc "NC" "nc"
    
    # openpgm Messungen
    Messen 10 $inc "OpenPGM" "pgm"

    # nrl norm Messungen
    Messen 10 $inc "NRLNORM" "norm"
done

echo "Ich habe fertig! :D"