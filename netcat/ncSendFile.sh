#!/bin/sh

n=1

while [ -d "$2/Messung$n" ]
do
    n=$((n+1))
done

mkdir "$2/Messung$n"

sleep 10

echo $(($(date +%s%N)/1000000)) > "starttime-"$(hostname)"-nc.txt"
mv "starttime-"* "$2/Messung$n/"

max="$1"
max=$((max))
Array=""

for i in `seq 1 $max`
do
    ncat -n 10.0.$i.10 1234 < file.txt &
    Array="$Array $!"
done

for i in $Array
do
    wait $i
    echo "$i"
done

echo $(($(date +%s%N)/1000000)) > "endtime-"$(hostname)"-nc.txt"
mv "endtime-"* "$2/Messung$n/"