#!/bin/sh

n=1

while [ -d "$1/Messung$n" ]
do
    n=$((n+1))
done

n=$((n-1))

wait

mv "starttime-"* "$1/Messung$n/"
mv "endtime-"* "$1/Messung$n/"

md5out=$(md5sum -c md5sum.txt)
if [ "$md5out" = "file.txt: OK" ]
then
    touch "$1/Messung$n/"$(hostname)"fileok.txt"
fi