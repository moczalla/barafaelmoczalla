#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

void errorMessage( char *message, ... ){
	
	va_list vl;
	va_start( vl, message );
	printf( "Error: " );
	vprintf( message, vl );
	printf( "\n" );
	fflush( stdout );
}

int main( int argc, char *argv[] ){
	
	FILE *data;
	
	if( !( data = fopen( "./test.foo", "w" ) ) ){
		
		errorMessage( "Opening file failed!" );
		return EXIT_FAILURE;
	}
	
	for( int j = 0; j < 10000; j++ ){
		
		for( int i = 0; i < 7995; i++ ){
			
			if( putc( '0', data ) == EOF ){
			
				errorMessage( "Writing '0' failed!" );
				return EXIT_FAILURE;
			}
		}
		
		if( putc( '\n', data ) == EOF ){
		
			errorMessage( "Writing '%cn' failed!", 92 );
			return EXIT_FAILURE;
		}
	}
	
	fclose( data );
	
	return EXIT_SUCCESS;
}

/* eof */
