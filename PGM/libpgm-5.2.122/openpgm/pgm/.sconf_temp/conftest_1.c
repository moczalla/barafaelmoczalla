

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char ftime();

int main() {
#if defined (__stub_ftime) || defined (__stub___ftime)
  fail fail fail
#else
  ftime();
#endif

  return 0;
}
