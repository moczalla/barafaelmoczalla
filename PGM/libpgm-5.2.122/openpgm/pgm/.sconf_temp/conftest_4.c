

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char getprotobyname_r();

int main() {
#if defined (__stub_getprotobyname_r) || defined (__stub___getprotobyname_r)
  fail fail fail
#else
  getprotobyname_r();
#endif

  return 0;
}
