#!/bin/sh

chmod a+x pgmFileRecv

sleep 2
#IP="$( ip -4 route get 8.8.8.8 | awk {'print $7'} | tr -d '\n' )"

n=1

while [ -d "$1/Messung$n" ]
do
    n=$((n+1))
done

n=$((n-1))

echo $(($(date +%s%N)/1000000)) > "starttime-"$(hostname)"-pgm.txt"
mv "starttime-"* "$1/Messung$n/"

DIR="$( pwd )"

./pgmFileRecv -file file.txt -intf "eth0" -addr "239.0.0.1" -udp -udp-port 7500 > log.txt

echo $(($(date +%s%N)/1000000)) > "endtime-"$(hostname)"-pgm.txt"
mv "endtime-"* "$1/Messung$n/"

md5out=$(md5sum -c md5sum.txt)
if [ "$md5out" = "file.txt: OK" ]
then
    touch "$1/Messung$n/"$(hostname)"fileok.txt"
else
    touch "$1/Messung$n/"$(hostname)"filenotok.txt"
fi