#!/bin/sh

filenameandloc="/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/Messungen.csv"

# MessungenZuTabelle "Anzahl der Messungen" "Anzahl der Teilnehmer" "Protokollkennung" "Protokollkennung in Messdatei"
MessungenZuTabelle() {

max=$1
n=$2
proto=$3

for i in `seq 1 $max`
do
    # Trage für Sender Startzeit ein
    if [ -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/starttime-n1-$4.txt" ]
    then
        # Trage für Sender Startzeit ein
        tmp=$(grep -Eo '[0-9]{13,13}' "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/starttime-n1-$4.txt")
        tmp2=$(echo "$proto,$n,$i,1,$tmp,")
    else
        # Trage für Sender Startzeit unendlich ein
        tmp2=$(echo "$proto,$n,$i,1,Inf,")
    fi
    
    # Trage für Sender Abschlusszeit ein
    if [ -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/endtime-n1-$4.txt" ]
    then
        # Trage für Sender Abschlusszeit ein
        tmp=$(grep -Eo '[0-9]{13,13}' "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/endtime-n1-$4.txt")
        echo "$tmp2$tmp,TRUE" >> $filenameandloc
    else
        # Trage für Sender Abschlusszeit unendlich ein
        echo $tmp2"Inf,TRUE" >> $filenameandloc
    fi
    
    temp=`expr $n + 1`
    for p in `seq 2 $temp`
    do
        # Trage für Empfänger Startzeit ein
        if [ -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/starttime-n$((p))-$4.txt" ]
        then
            # Trage für Empfänger Startzeit ein
            tmp=$(grep -Eo '[0-9]{13,13}' "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/starttime-n$p-$4.txt")
            tmp2=$(echo "$proto,$n,$i,$p,$tmp,")
        else
            # Trage für Empfänger Startzeit unendlich ein
            tmp2=$(echo "$proto,$n,$i,$p,Inf,")
        fi
        
        # Trage für Empfänger Abschlusszeit ein
        if [ -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/endtime-n$((p))-$4.txt" ]
        then
            # Trage für Empfänger Abschlusszeit ein
            tmp=$(grep -Eo '[0-9]{13,13}' "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/endtime-n$p-$4.txt")
            tmp3=$(echo "$tmp,")
        else
            # Trage für Empfänger Abschlusszeit unendlich ein
            tmp3=$(echo "Inf,")
        fi
        
        # Trage für Empfänger Dateistatus ein
        if [ -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/n$((p))fileok.txt" ]
        then
            # Trage für Empfänger Datei korrekt erhalten ein
            echo $tmp2$tmp3"TRUE" >> $filenameandloc
        else
            # Trage für Empfänger Datei nicht korrekt erhalten ein
            echo $tmp2$tmp3"FALSE" >> $filenameandloc
        fi
    done
done
}

recvnums="1 5 10 15 20 25 30 40 50" # 60 70 80 90 100 200 1000"

echo "ProtokollID,Empfanzahl,Messungsnr,TeilnehmerID,Startzeit,Endzeit,DateiOK" > "$filenameandloc"

for inc in $recvnums
do
    # nc Messungen
    MessungenZuTabelle 10 $inc "NC" "nc"
    
    # openpgm Messungen
    MessungenZuTabelle 10 $inc "OpenPGM" "pgm"

    # nrl norm Messungen
    MessungenZuTabelle 10 $inc "NRLNORM" "norm"
done

#Rscript RAnalysis.r

echo "Ich habe fertig! :D"
