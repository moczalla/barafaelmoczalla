

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char gettimeofday();

int main() {
#if defined (__stub_gettimeofday) || defined (__stub___gettimeofday)
  fail fail fail
#else
  gettimeofday();
#endif

  return 0;
}
