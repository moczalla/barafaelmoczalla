

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char backtrace();

int main() {
#if defined (__stub_backtrace) || defined (__stub___backtrace)
  fail fail fail
#else
  backtrace();
#endif

  return 0;
}
