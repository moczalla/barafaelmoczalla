#!/bin/sh

sleep 2

n=1

while [ -d "$1/Messung$n" ]
do
    n=$((n+1))
done

n=$((n-1))

echo $(($(date +%s%N)/1000000)) > "starttime-"$(hostname)"-nc.txt"
mv "starttime-"* "$1/Messung$n/"

ncat -l -p 1234 > file.txt

echo $(($(date +%s%N)/1000000)) > "endtime-"$(hostname)"-nc.txt"
mv "endtime-"* "$1/Messung$n/"

md5out=$(md5sum -c md5sum.txt)
if [ "$md5out" = "file.txt: OK" ]
then
    touch "$1/Messung$n/"$(hostname)"fileok.txt"
else
    touch "$1/Messung$n/"$(hostname)"filenotok.txt"
fi