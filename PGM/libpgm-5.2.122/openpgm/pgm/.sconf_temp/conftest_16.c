

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char getifaddrs();

int main() {
#if defined (__stub_getifaddrs) || defined (__stub___getifaddrs)
  fail fail fail
#else
  getifaddrs();
#endif

  return 0;
}
