#!/bin/sh

date +%s > "starttime-nc-"$(hostname)".txt"
ncat -u -l 1234 > recv.txt &
wait
date +%s > "endtime-nc-"$(hostname)".txt"