

#include <assert.h>

#ifdef __cplusplus
extern "C"
#endif
char getnetent();

int main() {
#if defined (__stub_getnetent) || defined (__stub___getnetent)
  fail fail fail
#else
  getnetent();
#endif

  return 0;
}
