#!/bin/sh

chmod a+x normFileSend
chmod a+x norm

n=1

while [ -d "$1/Messung$n" ]
do
    n=$((n+1))
done

mkdir "$1/Messung$n"

sleep 10

echo $(($(date +%s%N)/1000000)) > "starttime-"$(hostname)"-norm.txt"
mv "starttime-"* "$1/Messung$n/"

./normFileSend file.txt

echo $(($(date +%s%N)/1000000)) > "endtime-"$(hostname)"-norm.txt"
mv "endtime-"* "$1/Messung$n/"