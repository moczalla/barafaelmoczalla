#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#ifdef CONFIG_HAVE_EPOLL
#	include <sys/epoll.h>
#endif
#include <sys/types.h>
#ifndef _WIN32
#	include <inttypes.h>
#	include <unistd.h>
#	include <netdb.h>
#	include <netinet/in.h>
#	include <sched.h>
#	include <sys/socket.h>
#	include <arpa/inet.h>
#	include <sys/time.h>
#else
#	include <ws2tcpip.h>
#	include <mswsock.h>
#	include <pgm/wininttypes.h>
#	include "getopt.h"
#endif
#include <glib.h>
#include <pgm/pgm.h>
#ifdef CONFIG_WITH_HTTP
#	include <pgm/http.h>
#endif
#ifdef CONFIG_WITH_SNMP
#	include <pgm/snmp.h>
#endif

#ifndef MSG_ERRQUEUE
#	define MSG_ERRQUEUE		0x2000
#endif


static int		g_port = 0;
static char		g_network[26];
static bool		g_multicast_loop = TRUE;
static int		g_udp_encap_port;

static int		g_max_tpdu = 9000;
static int		g_max_rte = 400*1000;		/* very conservative rate, 2.5mb/s */
static int		g_sqns = 100;

static gboolean		g_fec = FALSE;
static int		g_k = 8;
static int		g_n = 255;

static pgm_sock_t*	g_sock = NULL;

static char		filename[ 100 ];


static gboolean create_pgm_socket(void);

int main( int	argc, char   *argv[] ){
	
	pgm_error_t *pgm_err = NULL;

	setlocale ( LC_ALL, "" );

/* pre-initialise PGM messages module to add hook for GLib logging */
	pgm_messages_init();
	
	if( !pgm_supported() ){
		
		if( !pgm_init( &pgm_err ) ){

			g_error( "Unable to start PGM engine: %s", pgm_err->message );
			pgm_error_free( pgm_err );
			pgm_messages_shutdown();
			return EXIT_FAILURE;
		}
	}else{
		
		g_error( "PGM engine already started!" );
		if( pgm_err ) pgm_error_free( pgm_err );
		pgm_messages_shutdown();
		return EXIT_FAILURE;
	}
	
	strcpy( g_network, "eth0;239.0.0.1" );
	g_udp_encap_port = 7500;
	
	const char* binary_name = strrchr( argv[0], '/' );
	int c;
	while( ( c = getopt ( argc, argv, "n:p:ih" ) ) != -1 ){
		
		switch(c) {
			case 'n':	
				if( optarg[ 0 ] != ':' )
					
					strcpy( g_network, optarg );
				break;

			case 'p':
				if( optarg[ 0 ] != ':' )
					
					g_udp_encap_port = atoi( optarg );
				break;

			case 'i':
				pgm_if_print_all();
				pgm_messages_shutdown();
				return EXIT_SUCCESS;

			case 'h':
			case '?':
				pgm_messages_shutdown();
				fprintf( stdout, "Usage: %s [options] message\n", binary_name );
				fprintf( stdout, "  -n <interface;network>	: Interface name or IP address and Multicast group or unicast IP address\n" );
				fprintf( stdout, "  -p <port>			: Encapsulate PGM in UDP on IP port\n" );
				fprintf( stdout, "  -i				: List available interfaces\n" );
				fprintf( stdout, "  -h				: Print help info text\n" );
				return EXIT_SUCCESS;
			default:
				fprintf( stderr, "Error: Reading arguments failed!\n" );
				return EXIT_FAILURE;
		}
	}

/* read file */
	size_t buflength = 8000;
	size_t elemtRead;
	char *buffer;
	FILE *file;
	
	file = fopen( "./test.foo", "r" );/*fopen( "../../Sender/debian-live-8.6.0-amd64-gnome-desktop.iso", "r" );*/
	
	if( !file ){
	
		printf( "Cannot open file!\n" );
		return EXIT_FAILURE;
	}
	
/* prepare for transfer */
	
	rewind( file );
	
	buffer = ( char * )malloc( ( buflength ) * sizeof( char ) );
	
	if( !buffer ){
		
		printf( "Error allocating space.\n" );
		return EXIT_FAILURE;
	}
	
	if( create_pgm_socket() ){
		
		int status;
		
		do{
			size_t bytes_written;
			
			elemtRead = fread( buffer, 1, buflength, file );
			
			//send file
			printf( "Sending file\n" );
			
			status = pgm_send( g_sock, buffer, elemtRead, &bytes_written );
			
			if( PGM_IO_STATUS_NORMAL != status ){
				
				g_error( "pgm_send failed." );
			}else{
				
				printf( "%d Bytes send.\n", bytes_written );
			}
		}while( elemtRead == buflength );
	}

	//cleanup
	
	fclose( file );
	
	if( g_sock ){

		pgm_close( g_sock, TRUE );
		g_sock = NULL;
	}

	pgm_shutdown();
	pgm_messages_shutdown();

	printf( "Exiting program!\n" );

	return EXIT_SUCCESS;
}


static gboolean create_pgm_socket( void ){
	struct pgm_addrinfo_t* res = NULL;
	pgm_error_t* pgm_err = NULL;
	sa_family_t sa_family = AF_INET;

/* parse network parameter into PGM socket address structure */
	if (!pgm_getaddrinfo (g_network, NULL, &res, &pgm_err)) {
		g_error ("Parsing network parameter: %s", pgm_err->message);
		goto err_abort;
	}

	sa_family = res->ai_send_addrs[0].gsr_group.ss_family;

	if (g_udp_encap_port) {
		if (!pgm_socket (&g_sock, sa_family, SOCK_SEQPACKET, IPPROTO_UDP, &pgm_err)) {
			g_error ("Creating PGM/UDP socket: %s", pgm_err->message);
			goto err_abort;
		}
		pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_UDP_ENCAP_UCAST_PORT, &g_udp_encap_port, sizeof(g_udp_encap_port));
		pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_UDP_ENCAP_MCAST_PORT, &g_udp_encap_port, sizeof(g_udp_encap_port));
	} else {
		if (!pgm_socket (&g_sock, sa_family, SOCK_SEQPACKET, IPPROTO_PGM, &pgm_err)) {
			g_error ("Creating PGM/IP socket: %s", pgm_err->message);
			goto err_abort;
		}
	}

/* Use RFC 2113 tagging for PGM Router Assist */
	const int no_router_assist = 0;
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_IP_ROUTER_ALERT, &no_router_assist, sizeof(no_router_assist));

	//pgm_drop_superuser();

/* set PGM parameters */
	const int send_only = 1,
		  ambient_spm = pgm_secs (30),
		  heartbeat_spm[] = { pgm_msecs (100),
				      pgm_msecs (100),
                                      pgm_msecs (100),
				      pgm_msecs (100),
				      pgm_msecs (1300),
				      pgm_secs  (7),
				      pgm_secs  (16),
				      pgm_secs  (25),
				      pgm_secs  (30) };

	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_SEND_ONLY, &send_only, sizeof(send_only));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_MTU, &g_max_tpdu, sizeof(g_max_tpdu));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_TXW_SQNS, &g_sqns, sizeof(g_sqns));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_TXW_MAX_RTE, &g_max_rte, sizeof(g_max_rte));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_AMBIENT_SPM, &ambient_spm, sizeof(ambient_spm));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_HEARTBEAT_SPM, &heartbeat_spm, sizeof(heartbeat_spm));
	if (g_fec) {
		struct pgm_fecinfo_t fecinfo; 
		fecinfo.block_size		= g_n;
		fecinfo.proactive_packets	= 0;
		fecinfo.group_size		= g_k;
		fecinfo.ondemand_parity_enabled	= TRUE;
		fecinfo.var_pktlen_enabled	= TRUE;
		pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_USE_FEC, &fecinfo, sizeof(fecinfo));
	}

/* create global session identifier */
	struct pgm_sockaddr_t addr;
	memset (&addr, 0, sizeof(addr));
	addr.sa_port = g_port ? g_port : DEFAULT_DATA_DESTINATION_PORT;
	addr.sa_addr.sport = DEFAULT_DATA_SOURCE_PORT;
	if (!pgm_gsi_create_from_hostname (&addr.sa_addr.gsi, &pgm_err)) {
		g_error ("Creating GSI: %s", pgm_err->message);
		goto err_abort;
	}

/* assign socket to specified address */
	struct pgm_interface_req_t if_req;
	memset (&if_req, 0, sizeof(if_req));
	if_req.ir_interface = res->ai_recv_addrs[0].gsr_interface;
	if_req.ir_scope_id  = 0;
	if (AF_INET6 == sa_family) {
		struct sockaddr_in6 sa6;
		memcpy (&sa6, &res->ai_recv_addrs[0].gsr_group, sizeof(sa6));
		if_req.ir_scope_id = sa6.sin6_scope_id;
	}
	if (!pgm_bind3 (g_sock,
			&addr, sizeof(addr),
			&if_req, sizeof(if_req),	/* tx interface */
			&if_req, sizeof(if_req),	/* rx interface */
			&pgm_err))
	{
		g_error ("Binding PGM socket: %s", pgm_err->message);
		goto err_abort;
	}

/* join IP multicast groups */
	for (unsigned i = 0; i < res->ai_recv_addrs_len; i++)
		pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_JOIN_GROUP, &res->ai_recv_addrs[i], sizeof(struct group_req));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_SEND_GROUP, &res->ai_send_addrs[0], sizeof(struct group_req));
	pgm_freeaddrinfo (res);

/* set IP parameters */
	const int blocking = 0,
		  multicast_loop = g_multicast_loop ? 1 : 0,
		  multicast_hops = 16,
		  dscp = 0x2e << 2;		/* Expedited Forwarding PHB for network elements, no ECN. */

	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_MULTICAST_LOOP, &multicast_loop, sizeof(multicast_loop));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_MULTICAST_HOPS, &multicast_hops, sizeof(multicast_hops));
	if (AF_INET6 != sa_family)
		pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_TOS, &dscp, sizeof(dscp));
	pgm_setsockopt (g_sock, IPPROTO_PGM, PGM_NOBLOCK, &blocking, sizeof(blocking));

	if (!pgm_connect (g_sock, &pgm_err)) {
		g_error ("Connecting PGM socket: %s", pgm_err->message);
		goto err_abort;
	}

	return TRUE;

err_abort:
	if (NULL != g_sock) {
		pgm_close (g_sock, FALSE);
		g_sock = NULL;
	}
	if (NULL != res) {
		pgm_freeaddrinfo (res);
		res = NULL;
	}
	if (NULL != pgm_err) {
		pgm_error_free (pgm_err);
		pgm_err = NULL;
	}
	return FALSE;
}

/* eof */
