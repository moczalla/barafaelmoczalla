
#if defined( __GNUC__ ) && ( defined( __i386__ ) || defined( __x86_64__ ) )
/* GCC assembler */
#elif defined( __sun )
/* Solaris intrinsic */
#elif defined( __APPLE__ )
/* Darwin intrinsic */
#elif defined( __GNUC__ ) && ( __GNUC__ * 100 + __GNUC_MINOR__ >= 401 )
/* GCC 4.0.1 intrinsic */
#elif defined( _WIN32 )
/* Windows intrinsic */
#else
#	errir "Unsupported atomic ops."
#endif
int
main ()
{
	return 0;
}
	