#!/bin/sh

chmod a+x norm

sudo xterm -hold -e 'tcpdump -vvX ip dst 239.0.0.1 > test.log' &

xterm -hold -e './norm addr "239.0.0.1/7002" rxcachedir ./recv' &
sleep 1
xterm -hold -e './norm addr "239.0.0.1/7002" rate 5000000 send foo.txt' &

sudo tcpdump -vvX ip dst 239.0.0.1
