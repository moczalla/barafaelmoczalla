#!/bin/sh

n=1
i=1
proto="NC"

a=0
    
while [ $a -lt 1 ]
do
    t=1
    temp=`expr $n + 1`
    for p in `seq 2 $temp`
    do
        if [ ! -f "/home/praktikum/Dokumente/BachelorarbeitMoczalla/BAGit/Messungen/$proto/50mbps/$((n))Empf/Messung$i/n$((p))fileok.txt" ]
        then
            t=0
        fi
    done
    
    if [ $t -eq 1 ]
    then
        echo "continue"
        a=`expr $a + 1`
        sleep 10 &
        PID=$!
        wait $PID
    else
        echo "sleeping wait"
        sleep 10 &
        PID=$!
        wait $PID
    fi
done

echo $(($(date +%s%N)/1000000)) > "starttime-"$(hostname)"-nc.txt"
sleep 6 &
PID1=$!
sleep 10 &
wait $PID1
echo $(($(date +%s%N)/1000000)) > "endtime-"$(hostname)"-nc.txt"